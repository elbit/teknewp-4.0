<?php get_header(); ?>

<section class="uk-padding-top home-banner">
	<?php get_template_part( 'template-parts/front-page/teknecultura/hero' ); ?>
</section>

<section id="project" class="uk-section home-content">
	<?php get_template_part( 'template-parts/front-page/teknecultura/home-content-parallax' ); ?>
</section>

<section id="" class="uk-section-xsmall home-switch-banner uk-background-secondary-soft t-w" >
	<?php get_template_part( 'template-parts/front-page/teknecultura/home-switch-banner' ); ?>
</section>

<?php get_template_part( 'template-parts/front-page/shared/front-page-shared' ); ?>


<?php get_footer(); ?>


