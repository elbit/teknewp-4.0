<?php get_header(); ?>

<main role="main" aria-label="Content">

<div class="uk-section uk-section-small uk-margin-small-bottom uk-padding-remove-bottom">
	<div class="uk-container" >
		<?php get_template_part( 'searchform' ); ?>
		<?php get_template_part( 'category-list' ); ?>
	</div>
	
</div>

	<section class="uk-section uk-section-small uk-padding-remove-top">
		
		<div class="uk-container" >
		<h1> <?php echo sprintf( __( '%s Search Results for ', 'html5blank' ), $wp_query->found_posts ); echo get_search_query(); ?> </h1>
			<div id="infinite-container" class="uk-child-width-1-2@s  uk-grid-match "uk-grid>
				<?php get_template_part( 'loop' ); ?>
				<?php //get_template_part( 'pagination' ); ?>
			
			</div>
		</div>
	</section>
	
</main>

<?php get_footer(); ?>