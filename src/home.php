<?php get_header(); ?>

<main role="main" aria-label="Content">

<div class="uk-section uk-section-small uk-margin-small-bottom uk-padding-remove-bottom">
	<div class="uk-container" >
		<?php get_template_part( 'searchform' ); ?>
		<?php get_template_part( 'category-list' ); ?>
	</div>
</div>

	<section class="uk-section uk-section-small">
		<div class="uk-container" >
			<div id="infinite-container" class="uk-child-width-1-2@s  uk-grid-match "uk-grid>
				
				<?php get_template_part( 'loop' ); ?>
				<?php //get_template_part( 'pagination' ); ?>
			
			</div>
		</div>
	</section>
	
</main>

<?php get_footer(); ?>