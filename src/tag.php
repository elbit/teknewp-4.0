<?php get_header(); ?>
<main role="main" aria-label="Content">
	
	
	
	<section class="uk-section uk-section-small">
		<div class="uk-container" >
			<h1 class="bit-page-title"><?php esc_html_e( 'Tag Archive: ', 'html5blank' ); echo single_tag_title( '#', false ); ?></h1>
			<div id="infinite-container"class="uk-child-width-1-2 uk-grid-large uk-grid-match "uk-grid>
					

					<?php get_template_part( 'loop' ); ?>
					<?php //get_template_part( 'pagination' ); ?>
			
			</div>
		</div>
	</section>
	
	
</main>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
<?php get_footer(); ?>









