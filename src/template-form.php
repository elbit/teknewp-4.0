<?php /* Template Name: template form */ get_header(); ?>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<!-- article -->
			<div class="uk-container uk-clearfix" ">
			
				<div class="fs-form-wrap uk-margin-top uk-clearfix" id="fs-form-wrap" style="height: 95vh;">
					

					<?php the_content(); ?>

					

				</div>

			</div>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else : ?>

		

		<?php endif; ?>

		
		<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/lib/FullscreenForm/js/classie.js"></script>
		
		<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/lib/FullscreenForm/js/selectFx.js"></script>
		
		<script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/lib/FullscreenForm/js/fullscreenForm.js"></script>
		
		<script>
			(function() {
				var formWrap = document.getElementById( 'fs-form-wrap' );

				[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
					new SelectFx( el, {
						stickyPlaceholder: false,
						onChange: function(val){
							document.querySelector('span.cs-placeholder').style.backgroundColor = val;
						}
					});
				} );

				new FForm( formWrap, {
					onReview : function() {
						classie.add( document.body, 'overview' ); // for demo purposes only
					}
				} );
			})();
		</script>

<?php get_footer(); ?>



