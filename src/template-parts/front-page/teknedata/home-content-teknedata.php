<?php
$loop = new WP_Query( array(
'post_type' => 'home_content_tkdata',
'posts_per_page' => -1
)
);
?>

<div class="uk-container uk-container-large bit-serveis bit-serveis-tkd" style="padding-top: 0;">
	
	
	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

		<?php

			for ($u = 1; $u <= 6; $u++) {

  				$h = "avant_titol_$u";
  				$i = "bloc_$u";
  				$j = "$u";

				$f = get_field($h);
  				$g = get_field($i);
  				$img = get_field($i);

  				$img_url = get_template_directory_uri();


  				//echo "avant_title_$u";
    		echo '<section class="uk-section before-animate uk-margin-remove-top bit-servei bit-servei-tkd"  uk-scrollspy="cls:animate; repeat:true; delay: 400;" >
		
					<div class="uk-flex uk-flex-between uk-flex-middle servei bit-servei-wrapper uk-position-relative" uk-grid>
						<div class="bit-servei-module bit-servei-module-tkd bit-servei-content bit-servei-content-tkd uk-width-2-3@m" >
						'; 
	
    		echo '			<h3 class="uk-text-secondary bit-servei-title">'.$f.'</h3>';
			echo '			<hr class="hr-teknedata">
				
							<div class="">'.$g.'</div>
				
						</div>
						
			
						
						<div class="bit-servei-module bit-servei-paralax plx1 uk-position-relative servei-img uk-flex uk-flex-center uk-flex-middle bit-servei-img-tkd uk-width-1-3@m" style="min-height: 200px;">
							<img class="" src="';

			 echo $img_url ;

			 echo '/img/continguts/teknedata/'.$u.'.svg" >

						</div>
						
			
					</div>
		
				</section>';
    
  }
?>

<?php endwhile; wp_reset_query(); ?>

<?php get_template_part( 'template-parts/front-page/teknedata/sponsor' ); ?>

</div>


<script src="https://cdn.jsdelivr.net/npm/fitty@2.2.6/dist/fitty.min.js"></script>
<script>
fitty('.sub-claim',{

	//multiLine:true,
	maxSize: 40,
	minSize	:20
}) ;
</script>