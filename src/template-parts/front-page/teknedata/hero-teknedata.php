
<div class="uk-container uk-container-large bit-hero-tkd" >
	
	<div class="uk-child-width-1-2@s uk-flex-middle " uk-height-viewport="offset-top: true;" uk-grid>
		
		<div class="bit-hero-tkd-l" style="margin-top: -5%; ">

			<h1 class="bit-hero-title uk-text-bold claim" ><?php pll_e('los <span class="uk-text-secondary" >datos </span> de tu <span class="uk-text-secondary" >audiencia <br></span>a tiempo real '); ?></h1>
			
			<h2 class="uk-text-lead uk-margin-bottom uk-text-normal bit-hero-subtitle "><span class="sub-claim"><?php pll_e('software de <i>Business Intelligence </i>para el sector cultural') ?> </span></h2>
			
			<div class="bit-hero-ctas uk-child-width-1-2@m uk-grid-small" uk-grid>
				
				<div>
					<a href="<?php echo esc_url( home_url( '/' ) ); ?><?php pll_e('contacto'); ?>/" class="uk-button uk-button-secondary  uk-text-bold uk-width-1-1"  ><?php pll_e('solicita demo') ?></a>
				</div>

				<div class="uk-display-inline"  uk-lightbox="animation: slide">

					<?php if(ICL_LANGUAGE_CODE=='es'): ?>
							

							<a class="uk-button uk-button-default uk-text-bold uk-width-1-1 " href="https://youtu.be/cv4qOU080LQ"><?php pll_e('dentro vídeo') ?> <span class="uk-text-middle " uk-icon="icon:  play-circle; ratio:1.3;" > </span></a>

						<?php elseif(ICL_LANGUAGE_CODE=='ca'): ?>
						<a class="uk-button uk-button-default uk-text-bold uk-width-1-1 " href="https://youtu.be/xv-qNMOkPrE">obrir video <span class="uk-text-middle " uk-icon="icon:  play-circle; ratio:1.3;" > </span></a>
					<?php endif; ?>
					
				</div>


			</div>
			
		</div>
	
		<div class="bit-hero-tkd-r uk-flex  uk-flex-middle uk-flex-center">
				<img src="<?php echo get_template_directory_uri() ?>/img/banner/tkd-bg.svg" alt=""   >
		</div>

	</div>
	
</div>

<script src="https://cdn.jsdelivr.net/npm/fitty@2.2.6/dist/fitty.min.js"></script>
<script>
fitty('.sub-claim',{

	multiLine:true,
	minSize : 23,
	maxSize	:30
}) ;
fitty('.claim',{

multiLine:true,
minSize : 40,
maxSize	:60
}) ;
</script>