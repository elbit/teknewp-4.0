<section class="uk-section before-animate uk-margin-remove-top bit-servei bit-servei-tkd"  uk-scrollspy="cls:animate; repeat:true; delay: 400;" style="padding-top: 0;">
    <h3 class="uk-text-secondary bit-servei-title"> <?php pll_e('Teknedata es un proyecto subvencionado por:')?></h3>
		<hr class="hr-teknedata">
    <a href="http://www.culturaydeporte.gob.es/cultura.html" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknedata/ministerio-de-cultura.png" alt="ministerio de cultura"></a>
       
</section>