<?php
	$loop = new WP_Query( array(
	'post_type' => 'membres_equip',
	'posts_per_page' => -1,
	'order' => 'ASC'
	)
	);
?>
<div class="uk-container uk-position-relative">
	
	<h2 class="uk-text-center uk-text-bold uk-margin-large-bottom home-section-title"><?php pll_e('Nosotros somos'); ?></h2>
	
	<div uk-slider="">
		
		<a class="uk-position-center-right uk-margin-right uk-position-z-index" href="#" uk-slider-item="next" ><span uk-icon="icon: chevron-right; ratio:3;"></span></a>

		<a class="uk-position-center-left uk-margin-left uk-position-z-index" href="#" uk-slider-item="previous" ><span uk-icon="icon: chevron-left; ratio:3;"></span></a>

		<div class="uk-slider-container uk-position-relative">
		
			<div class="uk-child-width-1-2@s uk-child-width-1-4@m  uk-slider-items uk-grid-small"  uk-grid>
				
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<?php
				$image = get_field('imatge');
				$size = 'thumbnail'; // (thumbnail, medium, large, full or custom size)
				?>
				
				<ul class="uk-list uk-text-center" >
					
					<li><?php echo wp_get_attachment_image( $image, $size,"",array( "class" => "uk-comment-avatar uk-border-circle  bit-u-blackandwhite" ) ); ?></li>
					
					<li><h4 class="uk-text-bold"><?php the_title()?></h4></li>
					
					<li><?php the_field('carrec') ?> </li>
					
					<li>
						
						<?php if( get_field('twitter') ): ?>
						<a href="<?php the_field('twitter') ?>" class="uk-icon-button color-tw" uk-icon="icon: twitter;" target="_blank" rel=”noopener noreferrer”></a>
						<?php endif; ?>
						
						<?php if( get_field('Linkedin') ): ?>
						<a href="<?php the_field('Linkedin') ?>" class="uk-icon-button color-lkd"" uk-icon="icon: linkedin;" target="_blank" rel=”noopener noreferrer”></a>
						<?php endif; ?>
					
					</li>
					
					<li><blockquote><?php //the_field('descripcio') ?></blockquote></li>
					
				</ul>
				
				<?php endwhile; wp_reset_query(); ?>
				
			</div>
		
		</div>
		
		<ul class="uk-slider-nav uk-dotnav uk-margin-top uk-flex-center"></ul>
	
	</div>
</div>

