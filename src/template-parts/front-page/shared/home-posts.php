<?php
	$loop = new WP_Query( array(
	'post_type' => 'post',
	'posts_per_page' => 8
	)
	);
?>
<div class="uk-container uk-container-expand  uk-position-relative front-page-posts-wrapper ">

	<h2 class="uk-text-bold uk-text-center uk-margin-remove-bottom home-section-title ">Blog </h2> 

	<a class="uk-button uk-display-block uk-text-center uk-text-bold uk-margin-medium-bottom event-read-more-home-blog" href="<?php echo esc_url( home_url( '/' ) ); ?><?php pll_e('blog/'); ?>"> <?php pll_e('ver todos los artículos del blog '); ?><span class="uk-text-baseline" uk-icon="icon:chevron-right; ratio:1;" > </span></a>
	
	<div class=" uk-margin-medium-top fp-posts-slider"  uk-slider="">

		<div class="uk-slider-container uk-position-relative bgsh" >

			<div class="uk-grid-large  uk-slider-items"  uk-grid>
				
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
				
				<div class="front-page-post" style="">

						<div class=""><?php the_post_thumbnail(  ) ?></div>
						
						<h3 class="uk-margin-remove-bottom uk-margin-small-top uk-text-break home-post-title"><a href=" <?php the_permalink() ?>"><?php the_title() ?></a></h3>
						
						<h6 class="uk-text-muted uk-text uk-margin-remove-top uk-margin-small-bottom meta"><?php //the_author() ?> <?php the_date( $d = '', $before = '', $after = '', $echo = true ) ?></h6>
						
						<div class="uk-text-small uk-margin-bottom"><?php echo(get_the_excerpt()); ?></div>
						
					</div>

				<?php endwhile; wp_reset_query(); ?>
			
			</div>

		</div>
		
		<a class="uk-position-center-left" href="#" uk-slider-item="previous" ><span uk-icon="icon: chevron-left; ratio:3.2;"></span></a>
		<a class="uk-position-center-right " href="#" uk-slider-item="next" >
			<span uk-icon="icon: chevron-right; ratio:3.2;"></span>
		</a>
		
		<ul class="uk-slider-nav uk-dotnav uk-margin-medium-top uk-flex-center"></ul>
	
	</div>

</div>

