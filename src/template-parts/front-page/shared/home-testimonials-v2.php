<?php
	$loop = new WP_Query( array(
	'post_type' => 'opinions_clients',
	'posts_per_page' => -1
	)
	);
?>


<div id="cases" class="uk-container uk-container-expand uk-margin-large-top uk-position-relative home-testimonials bit-testimonials" >
	
	<h2 class="uk-text-center uk-text-bold home-section-title"><?php pll_e('y esto es lo que opinan'); ?></h2>
	
	<div class="" uk-slider="center: true">
		
		<div class="uk-slider-container uk-position-relative" >

			<div class="uk-slider-items uk-flex uk-flex-middle uk-child-width-1-1" >
				
				<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
				
				<?php
					$image = get_field('imatge');
					$size = 'thumbnail'; 
				?>
				
				<div class="uk-slider-item">
					
					<div class="uk-margin-remove-top uk-flex uk-flex-column uk-flex-middle " style="">
						
						<ul class="uk-list uk-list uk-text-center uk-margin-remove-bottom uk-margin-remove-left" style="" >
									
							<li class="">
								<?php echo wp_get_attachment_image( $image, $size,"",array( "class" => "uk-border-circle" ) ); ?>
							</li>
							
							<li><h4 class="uk-margin-remove-top uk-text-bold"><?php the_title()?></h4></li>
							
							<li class="uk-margin-remove-top" ><?php the_field('carrec') ?> <br>
								<?php the_field('data') ?> </li>
							
							
						</ul>
							
						<?php if(ICL_LANGUAGE_CODE=='es'): ?>

							<div class="uk-position-relative uk-overflow-hidden readmore">
								
								<span class="uk-text-large uk-text-bold uk-position-top-right" style="" uk-icon="icon: quote-right; ratio: 1.2" ></span>
								
								<blockquote class="uk-text-center bit-testimonials-blockquote" style="">
									
									<?php the_field('opinio') ?>
		
								</blockquote>
						
							</div>
							
							<?php elseif(ICL_LANGUAGE_CODE=='ca'): ?>
							<div class="uk-position-relative uk-overflow-hidden readmore-cat">
							
							<span class="uk-text-large uk-text-bold uk-position-top-right" style="" uk-icon="icon: quote-right; ratio: 1.2" ></span>
							
							<blockquote class="uk-text-center bit-testimonials-blockquote" style="">
								
								<?php the_field('opinio') ?>
	
							</blockquote>
						
						</div>
							<?php endif; ?>

					</div>
				</div>
				
				<?php endwhile; wp_reset_query(); ?>
				
			</div>
		</div>

		<a class="uk-position-center-left" href="#" uk-slider-item="previous" ><span uk-icon="icon: chevron-left; ratio:3.2;"></span></a>
		
		<a class="uk-position-center-right " href="#" uk-slider-item="next" >
			<span uk-icon="icon: chevron-right; ratio:3.2;"></span>
		</a>
		
		<ul class="uk-slider-nav uk-dotnav uk-margin-medium-top uk-flex-center"></ul>
	
	</div>

</div>

<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/Readmore.js/2.2.1/readmore.min.js"></script>

<script>

	$('.readmore').readmore({
		
  		moreLink: '<a class="uk-text-bold uk-text-small " style="font-size: 0.8rem;" href="#">Leer más</a>',
  		lessLink: '<a class="uk-text-bold uk-text-small" style="font-size: 0.8rem;" href="#">Cerrar</a>',
  		collapsedHeight: 120,
  		//heightMargin: 20,
  		startOpen: false
	});
	$('.readmore-cat').readmore({
		
  		moreLink: '<a class="uk-text-bold uk-text-small " style="font-size: 0.8rem;" href="#">Llegir més</a>',
  		lessLink: '<a class="uk-text-bold uk-text-small" style="font-size: 0.8rem;" href="#">Tancar</a>',
  		collapsedHeight: 120,
  		//heightMargin: 20,
  		startOpen: false
	});

</script>



