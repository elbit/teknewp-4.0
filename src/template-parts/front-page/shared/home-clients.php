<div id="clients" class="uk-container uk-container-expand  uk-position-relative ">
	<h2 class="uk-text-bold uk-text-center uk-margin-large-bottom home-section-title"><?php pll_e('Ya utilizan sus datos'); ?></h2>
	
	<div class="uk-margin-medium-right home-clients"  uk-slider="sets: true;">
		<div class="uk-slider-container uk-position-relative bgsh">

			<style>.home-clients-slider img { max-height: 65px;}
			</style>
			
			<ul class="uk-grid-medium uk-slider-items uk-flex-middle home-clients-slider"  uk-grid>
				
				<li><a href="http://atriumviladecans.com" target="_blank" rel=”noopener noreferrer”>
					<img src="https://i2.wp.com/teknecultura.com/wp-content/uploads/2016/11/atrium_400x200.png" alt=""> 
				</a>
				</li>
				<li><a href="https://bito.pro/" target="_blank" rel=”noopener noreferrer”>
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/clients/bito-400-200.png" alt=""> 
				</a>
				</li>
				<li><a href="https://www.temporada-alta.com/" target="_blank" rel=”noopener noreferrer”>
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/clients/temporada-alta-400-200.png" alt=""> 
				</a>
				</li>
				<li><a href="https://www.teatroscanal.com/" target="_blank" rel=”noopener noreferrer”>
					<img src="https://i2.wp.com/teknecultura.com/wp-content/uploads/2016/11/Teatros_canal_400x200.png" alt=""> 
				</a>
				</li>
				<li><a href="https://pentacion.com/" target="_blank" rel=”noopener noreferrer”>
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/clients/pentacion.png" alt=""> 
				</a>
				</li>
				
				<li><a href="https://www.zaragoza.es/sedeelectronica/" target="_blank" rel=”noopener noreferrer”>
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/clients/zaragoza.png" alt=""> 
				</a>
				</li>
				<li><a href="https://www.fundaciocatalunya-lapedrera.com/es/home" target="_blank" rel=”noopener noreferrer”>
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/clients/pedrera-new.png" alt=""> 
				</a>
				</li>
				<li><a href="https://www.firatarrega.cat" target="_blank" rel=”noopener noreferrer”>
					<img src="https://i2.wp.com/teknecultura.com/wp-content/uploads/2016/11/tarrega_400x200.png" alt=""> 
				</a>
				</li>
				<li><a href="https://www.auditori.cat" target="_blank" rel=”noopener noreferrer”>
					<img src="https://i1.wp.com/teknecultura.com/wp-content/uploads/2016/11/auditori_400x200.jpg" alt="">
				</a>
				</li>
				<li><a href="http://www.anticteatre.com" target="_blank" rel=”noopener noreferrer”>
					<img src="https://i1.wp.com/teknecultura.com/wp-content/uploads/2016/11/antic-teatre_400x200.jpg" alt="">
				</a>
				</li>
				<li><a href="https://www.cofae.net/" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2016/11/cofae_400x200.png" alt="">
				</a>
				</li>
				<li><a href="http://conca.gencat.cat/ca/inici" target="_blank" rel=”noopener noreferrer”>
					<img src="https://i1.wp.com/teknecultura.com/wp-content/uploads/2016/11/conca_400x200.jpg" alt="">
				</a>
				</li>
				<li><a href="https://latlantidavic.cat" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2016/11/atlantida_400x200.png" alt="">
				</a>
				</li>
				<li><a href="http://www.dagolldagom.com" target="_blank" rel=”noopener noreferrer”>
					<img src="https://i1.wp.com/teknecultura.com/wp-content/uploads/2016/11/Dagoll_400x200.jpg" alt="">
				</a>
				</li>
				<li><a href="https://www.diba.cat" target="_blank" rel=”noopener noreferrer”>
					<img src="https://i2.wp.com/teknecultura.com/wp-content/uploads/2016/11/Diba_400x200.jpg" alt="">
				</a>
				</li>
				<li><a href="http://www.filmoteca.cat/web/" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2016/11/filmoteca_400x200.png" alt="">
				</a>
				</li>
				
				
				<li><a href="http://www.focus.es/" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2016/11/focus_400x200.png" alt="">
				</a>
				</li>
				
				<li><a href="https://web.gencat.cat/ca/inici/" target="_blank" rel=”noopener noreferrer”>
					<img src="https://i0.wp.com/teknecultura.com/wp-content/uploads/2016/11/Generalitat_400x200.png" alt="">
				</a>
				</li>
				<li><a href="http://www.gestorcultural.org/" target="_blank" rel=”noopener noreferrer”>
					</a><img src="https://i0.wp.com/teknecultura.com/wp-content/uploads/2016/11/gestors.png" alt
					="">

				</li>
				<li><a href="https://www.teatreauditoridegranollers.cat/" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2016/11/granollers-250x76.png" alt="">
</a>
					
				</li>
				<li><a href="https://www.ibercamera.com/es" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2018/02/ibercamera.jpg" alt="">
</a>
					
				</li>
				<li><a href="https://www.kursaal.cat/" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2016/11/kursaal_400x200.jpg" alt="">
</a>
					
				</li>
				<li><a href="https://www.liceubarcelona.cat/" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2016/11/liceu.png" alt="" style="max-height: 80px;">
</a>
					
				</li>
				<li><a href="https://www.teatrelliure.com/" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2016/11/logo-teatre-lliure.png" alt="">
</a>
					
				</li>
				<li><a href="https://www.tresc.cat/" target="_blank" rel=”noopener noreferrer”>
					<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/clients/tresC.png" alt="">
</a>
					
				</li>
				<li><a href="http://www.mmb.cat" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2016/11/maritim.png" alt="">
</a>
					
				</li>
				<li><a href="https://www.lesarts.com/" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2018/02/palaulogo-400X200-1.png" alt="">
</a>
					
				</li>
				<li><a href="http://mercatflors.cat" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2016/11/logo-mercat.png" alt="">
</a>
					
				</li>
				<li><a href="https://museuciencies.cat/" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2016/11/museu-blau-250x100.png" alt="" style="max-height: 60px;">
</a>
					
				</li>
				<li><a href="https://www.palaumusica.cat" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2016/11/palau.png" alt="">
</a>
					
				</li>
				<li><a href="https://www.teatrepoliorama.com/" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2016/11/poliorama_400x200.png" alt="">
				</a>
				</li>


				<li><a href="" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2016/11/sant-cugat-250x92.png" alt="">
				</a>
				</li>


				<li><a href="http://tasantcugat.cat/" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2016/11/tenerife_400x200.png" alt="">
				</a>
				</li>
				<li><a href="https://www.tnc.cat/" target="_blank" rel=”noopener noreferrer”>
					<img src="https://teknecultura.com/wp-content/uploads/2016/11/TNC_400x200.jpg" alt="">
				</a>
				</li>
				
				
				<li><a href="https://www.teatrevictoria.com/" target="_blank" rel=”noopener noreferrer”>
					<img src="https://i0.wp.com/teknecultura.com/wp-content/uploads/2016/11/victoria.png" alt="">
				</a>
				</li>
			
			</ul>
			
		</div>

		<a class="uk-position-center-left" href="#" uk-slider-item="previous" ><span uk-icon="icon: chevron-left; ratio:2;"></span></a>
		<a class="uk-position-center-right uk-margin-small-right" href="#" uk-slider-item="next" >
			<span uk-icon="icon: chevron-right; ratio:2;"></span>
		</a>
		
		<ul class="uk-slider-nav uk-dotnav uk-margin-medium-top uk-flex-center"></ul>
	
	</div>

</div>