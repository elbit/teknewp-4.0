<div class="fp-shared">
<section id="team" class="uk-section  uk-section-muted home-team ">
	<?php get_template_part( 'template-parts/front-page/shared/home-team' ); ?>
</section>

<section  class="uk-section">
	<?php get_template_part( 'template-parts/front-page/shared/home-clients' ); ?>
	<?php get_template_part( 'template-parts/front-page/shared/home-testimonials-v2' ); ?>
</section>

<section id="blog" class="uk-section home-posts" >


	<?php get_template_part( 'template-parts/front-page/shared/home-posts' ); ?>
</section>
</div>