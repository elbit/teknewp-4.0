<?php
$loop = new WP_Query( array(
'post_type' => 'home_content',
'posts_per_page' => -1
)
);
?>
<div class="uk-container uk-container-large bit-serveis">
	
	<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>

		<?php get_template_part( 'template-parts/front-page/teknecultura/paralax/paralax-1' ); ?>
		<?php get_template_part( 'template-parts/front-page/teknecultura/paralax/paralax-2' ); ?>
		<?php get_template_part( 'template-parts/front-page/teknecultura/paralax/paralax-3' ); ?>
		<?php get_template_part( 'template-parts/front-page/teknecultura/paralax/paralax-4' ); ?>
		<?php get_template_part( 'template-parts/front-page/teknecultura/paralax/paralax-5' ); ?>
		<?php get_template_part( 'template-parts/front-page/teknecultura/paralax/paralax-6' ); ?>

	
	<?php endwhile; wp_reset_query(); ?>

</div>
