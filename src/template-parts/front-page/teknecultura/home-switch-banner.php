<div class="uk-container uk-container-large uk-padding uk-position-relative bit-switch-banner" >
	
	<div class="uk-child-width-1-2 before-animate" uk-grid  uk-scrollspy="cls:animate; repeat:true; delay: 400;">
		
		<div class="uk-text-large uk-text-white bit-switch-banner-module">
			
			<h1><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-teknedata.svg" alt="Logo" class="logo-img"><!-- <span class="uk-text-bold">tekne</span><span class="uk-text-secondary uk-text-bold">data</span></a></h1> -->
			<h2 class="uk-margin-remove-top uk-text-bold"><?php pll_e('utiliza tus datos <span class="uk-text-secondary">en tiempo real</span>'); ?></h2>
			<h4 class="uk-margin-remove-top "><?php pll_e('software de <i>Business Intelligence</i> para el sector cultural '); ?>
			</h4>
			<h5 style="margin-top:0"><small><?php pll_e('Teknedata es un proyecto subvencionado por:')?></small></h5>
			<a href="http://www.culturaydeporte.gob.es/cultura.html" target="_blank"><img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknedata/ministerio-de-cultura-150.png" alt="ministerio de cultura"></a>
			
		</div>
		
		<div class="uk-flex uk-flex-center uk-flex-middle bit-switch-banner-module" >
			
			<img src="<?php echo get_template_directory_uri() ?>/img/tekne-responsive.svg" alt="">
			
		</div>
		
	</div>
	
	<div class=" uk-margin-large-top uk-child-width-1-3  uk-flex-around bit-switch-banner-btns" uk-grid>
		
		<div class="" >
			<a href="<?php echo esc_url( home_url( '/' ) ); ?><?php pll_e('contacto'); ?>/" class="uk-button uk-button-default  uk-text-bold uk-width-1-1"><?php pll_e('solicita demo'); ?></a>
		</div>


		<div class=" " uk-lightbox="animation: slide" >

					<?php if(ICL_LANGUAGE_CODE=='es'): ?>
						<a class="uk-button uk-button-default  uk-text-bold uk-width-1-1" href="https://youtu.be/cv4qOU080LQ"><span class=""uk-icon="icon:  play-circle;ratio:1;"> </span> <?php pll_e('Dentro vídeo'); ?></a>

					<?php elseif(ICL_LANGUAGE_CODE=='ca'): ?>
					<a class="uk-button uk-button-default  uk-text-bold uk-width-1-1" href="https://youtu.be/xv-qNMOkPrE"><span class=""uk-icon="icon:  play-circle;ratio:1;"> </span> obrir video</a>
				<?php endif; ?>
			
		</div>


		<div  class=""  >
			
			<a href="<?php echo esc_url( home_url( '/' ) ); ?><?php pll_e('teknedata/'); ?>" class="uk-button uk-button-default  uk-width-1-1">+ info <span class="uk-text-bold">tekne</span><span class=" uk-text-bold uk-text-secondary">data</span>
			</a>

		</div>
		
	</div>
	
	
</div>