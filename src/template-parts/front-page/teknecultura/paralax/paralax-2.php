<section class="uk-section uk-section-default uk-padding-remove-top bit-servei " >
		
		<div class="uk-child-width-1-2@m uk-flex uk-flex-between uk-flex-middle servei bit-servei-wrapper " >
			
			<div class="bit-servei-module bit-servei-content" style="boder:1px solid silver;">

				<h3 class="uk-text-primary  bit-servei-title" ><?php the_field('avant_titol_2') ?></h3>
				<hr>
				
				<div class=""><?php the_field('bloc_2') ?></div>

			</div>
			
			
			<div class="bit-servei-module bit-servei-paralax bit-servei-p2  uk-position-relative"  
			style="boder:1px solid silver; min-height: 350px; "
			>

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/2/bg.svg"  
				uk-parallax="y:0,80;scale:1; viewport:0.8""
				class="uk-position-top-right uk-position-absolute bit-servei-p2-bg"
				style="margin-right:0;">	
				
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/2/details.svg" 
				 uk-parallax="scale:1; x:-800; y:210; viewport:1.4"
				class="uk-position-top-left uk-position-absolute bit-servei-p2-d"
				style="margin-top: 3vh">

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/2/people.svg"  
				uk-parallax="scale:1;viewport:1; y:-60"
				class="uk-position-top-right uk-position-absolute bit-servei-p2-p" 
				style="margin-right:10vh; margin-top: 12vh; max-width: 550px;">

						
			</div>
			
		</div>
		
</section>