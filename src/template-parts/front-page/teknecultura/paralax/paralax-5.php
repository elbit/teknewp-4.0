<section class="uk-section uk-section-default uk-padding-remove-top bit-servei" >
		
		<div class="uk-child-width-1-2@s uk-flex uk-flex-between uk-flex-middle servei bit-servei-wrapper uk-position-relative" >
			
			<div class="bit-servei-module  bit-servei-content" style="borer:1px solid red;">

				<h3 class="uk-text-primary  bit-servei-title" ><?php the_field('avant_titol_5') ?></h3>
				<hr>
				
				<div class=""><?php the_field('bloc_5') ?></div>

			</div>
			
			<div class="bit-servei-module  bit-servei-paralax  bit-servei-paralax-5 uk-position-relative"  
			style="boder:1px solid silver; min-height: 350px; "
			ukx-parallax="y:100; viewport: 0.7">

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/5/bg.svg"  
				uk-parallax="y:150;scale:0.9;"
				class="uk-position-top-right uk-position-absolute"
				style="margin-right:0;margin-top:0vh;">	
				
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/5/people.svg"  
				uk-parallax=" viewport:1; y:100; scale:1;"
				class="uk-position-top-right uk-position-absolute" 
				style="margin-right:0; margin-top: 0vh; max-width: 450px;">

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/5/details.svg" 
				 uk-parallax="opacity:0.9; scale:1;"
				class="uk-position-top-left uk-position-absolute"
				style="margin-left:90%;margin-top: 10vh">

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/5/cogs.svg" 
				 uk-parallax="scale:1;rotate:360;"
				class="uk-position-top-left uk-position-absolute"
				style="margin-left:10%;margin-top:5vh">

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/5/arrow.svg"  
				uk-parallax="opacity:0.3;scale:0.5,1.5;y:-550; x:150; viewport:0.8; easing:0;"
				class="uk-position-top-left uk-position-absolute bit-servei-paralax-5-arrow"
				style="margin-left:14vh;margin-top: 20vh;">			
			</div>
			
		</div>
		
</section>