<section class="uk-section uk-section-default uk-padding-remove-top  bit-servei ">
		
		<div class="uk-child-width-1-2@m uk-flex uk-flex-between uk-flex-middle servei bit-servei-wrapper uk-position-relative" >
			
			<div class="bit-servei-module bit-servei-content" style="boder:1px solid red;">

				<h3 class="uk-text-primary  bit-servei-title" ><?php the_field('avant_titol_3') ?></h3>
				<hr>
				
				<div class=""><?php the_field('bloc_3') ?></div>

			</div>
			
			<div class="bit-servei-module bit-servei-paralax bit-servei-p3 uk-position-relative"  
			style="boder:1px solid silver; min-height: 350px;">

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/3/fons.svg"  
				uk-parallax="y:0,150;scale:1.1""
				class="uk-position-top-right uk-position-absolute bit-servei-p3-bg"
				style="margin-left:0;margin-top:-5vh;">
				
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/3/details.svg" 
				uk-parallax="scale:1; x:00; y:400; viewport:1.4"
				class="uk-position-top-right uk-position-absolute bit-servei-p3-d"
				style="margin-left:4vh;margin-top: -8vh">

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/3/elements.svg"  
				ukx-parallax="scale:1;viewport:1; y:250"
				class="uk-position-top-left uk-position-absolute bit-servei-p3-e" 
				style="margin-left:2vh;margin-top: 7vh;">

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/3/icona.svg" 
				uk-parallax="scale:0.9;y:50; x:-265%; viewport:0.7;easing:1.3;"
				class="uk-position-top-left uk-position-absolute bit-servei-p3-i"
				style=" margin-top: 8vh">

					
			</div>
			
		</div>
		
</section>