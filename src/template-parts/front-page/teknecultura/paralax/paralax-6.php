<section class="uk-section uk-section-default uk-padding-remove-top bit-servei uk-margin-xlarge-bottom"  >
		
		<div class="uk-child-width-1-2@m uk-flex uk-flex-between uk-flex-middle servei bit-servei-wrapper uk-position-relative" >
			
			<div class="bit-servei-module bit-servei-content" style="boder:1px solid red;">

				<h3 class="uk-text-primary  bit-servei-title" ><?php the_field('avant_titol_6') ?></h3>
				<hr>
				
				<div class=""><?php the_field('bloc_6') ?></div>

			</div>
			
			<div class="bit-servei-module  bit-servei-paralax plx1 uk-position-relative"  
			style="boder:1px solid silver; min-height: 350px; "
			>
				

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/6/cloud.svg" 
				 uk-parallax="scale:1; y:100; x:-400; viewport:1;"
				class="uk-position-top-right uk-position-absolute"
				style="margin-left:0vh;margin-top: -3vh">

				


				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/6/bg.svg"  
				uk-parallax="y:250;viewport:1; scale:1;"
				class="uk-position-top-right uk-position-absolute"
				style="margin-right:0vh;margin-top:-5vh;">

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/6/bg-2.svg"  
				uk-parallax="y:40;	 viewport:0.8; scale:0.8,1.1"
				class="uk-position-bottom uk-position-absolute"
				style="margin-right:3vh;margin-top:-10vh;">
				
				

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/6/people.svg"  
				uk-parallax=" scale:1;viewport:1; y:180"
				class="uk-position-top-right  uk-position-absolute" 
				style="margin-right:40%; margin-top: 0vh; max-width: 450px;">

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/6/icona.svg" 
				 uk-parallax="opacity: 0.5,1; scale:1.2; y:110"
				class="uk-position-top-right uk-position-absolute"
				style="margin-right:38%;margin-top: 8%">

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/6/direction.svg"  
				uk-parallax="y:100;viewport:0.8;scale:1.2;"
				class="uk-position-top-left uk-position-absolute"
				style="margin-right: :4vh;margin-top: 20vh;">			
			</div>
			
		</div>
		
</section>