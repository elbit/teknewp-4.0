<section class="uk-section uk-section-default uk-margin-remove-top bit-servei" style="padding-top: 0;">
		

		<div class="uk-child-width-1-2@m uk-flex uk-flex-between uk-flex-middle servei bit-servei-wrapper " >
			
			<div class="bit-servei-module bit-servei-content" >

				<h3 class="uk-text-primary  bit-servei-title" ><?php the_field('avant_titol_1') ?></h3>
				<hr>
				
				<div class=""><?php the_field('bloc_1') ?></div>

			</div>
			
			

			<div class="bit-servei-module bit-servei-paralax bit-servei-p1 uk-position-relative"  
			style="boder:1px solid silver;min-height: 350px;">

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/1/fons.svg"  
				uk-parallax=" scale:1;viewport:1; y:120"
				class="uk-position-top-right uk-position-absolute bit-servei-p1-bg"
				style="margin-right:0; margin-top: -2vh;">	
				
				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/1/espectadors.svg"  
				ukx-parallax="scale:1;viewport:1; y:200;viewport:0.9"
				class="uk-position-top-right uk-position-absolute bit-servei-p1-p" 
				style="margin-top: 25%; ">	

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/1/cog-1.svg" 
				 uk-parallax="scale:1;rotate:360;y:-190; viewport:0.8;x:50"
				class="uk-position-top-left uk-position-absolute bit-servei-p1-c1"
				style="margin-left:90%;margin-top: 31vh">

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/1/cog-2.svg" 
				 uk-parallax="scale:1;rotate:360;y:-190; viewport:0.8;x:50"
				class="uk-position-top-left uk-position-absolute bit-servei-p1-c2"
				style="margin-left:90%;margin-top: 30vh">

				<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/1/graph.svg"  
				uk-parallax="scale:1;rotate:360;y:290;x:50;"
				class="uk-position-top-left uk-position-absolute bit-servei-p1-g"
				style="margin-top: -5vh;">	

			</div>
			
		
		</div>
		
</section>