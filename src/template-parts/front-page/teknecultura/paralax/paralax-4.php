<section class="uk-section uk-section-large uk-padding-remove-top bit-servei"  >
		
		<div class="uk-child-width-1-2@m uk-flex uk-flex-between uk-flex-middle servei bit-servei-wrapper" >
			
			<div class="bit-servei-module  bit-servei-content">

				<h3 class="uk-text-primary bit-servei-title " ><?php the_field('avant_titol_4') ?></h3>
				<hr >
				
				<div class=""><?php the_field('bloc_4') ?></div>
				
			</div>
			
			<div class="bit-servei-module bit-servei-paralax  uk-position-relative bit-servei-p4 uk-height-medium" >
				
				<img class="servei-side- uk-position-absolute uk-position-top-right" 
				style="margin-top: 0vw;" 
				src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/4/fons.svg" 
				uk-parallax="y:0,100;scale:1.1" >

				<img class="servei-side- uk-position-absolute uk-position-top-left" 
				style="margin-top: -4vw; margin-left: 10vh" 
				src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/4/cloud.svg" 
				uk-parallax="y:0,150;scale:1" >
				
				<img class="servei-side- uk-position-absolute uk-position-top-right" 
				style="margin-right: 8vw" 
				src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/4/ordinador.svg"
				uk-parallax="y:0,150;scale:1">

				<img class="servei-side- uk-position-absolute uk-position-top-right" 
				style="margin-top: 4vw;" 
				src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/4/details.svg" 
				uk-parallax="y:0,-250;scale:1" >
				
				<img class="servei-side- uk-position-absolute uk-position-top-right bit-servei-p4-c" 
				style=" margin-top: 6vh;" 
				src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/continguts/teknecultura/4/cohet.svg" 
				
				uk-parallax="y:0,-500; scale:1; easing:1.7;">
			
			</div>
		
		</div>
		
	
	</section>
