<div class="uk-container uk-container-large bit-hero " uk-height-viewport="offset-top: true;">

	<div class="uk-child-width-1-2@s uk-flex uk-flex-left uk-flex-middle" uk-grid>
		
		<div class="" style="margin-top: -5%;boder: 1px solid red;" >

			<h1 class="bit-hero-title uk-text-bold" ><?php pll_e('utiliza tus <span class="uk-text-primary" >datos</span>') ?></h1>
			
			<h2 class="uk-text-lead uk-margin-bottom uk-text-normal bit-hero-subtitle "><span class="sub-claim"><?php pll_e(' convertimos datos en conocimiento útil para  el desarrollo  de proyectos culturales') ?></span></h2>
			
			<div class="bit-hero-ctas uk-child-width-1-1@m uk-child-width-1-2@l uk-grid-small" uk-grid>
				<div>
					<a href="#project" class="uk-button uk-button-primary uk-text-bold  uk-width-1-1 bit-btn"  uk-scroll="offset:60;">	<?php pll_e('empecemos') ?></a>
				</div>
				
				<div class="uk-display-inline"  uk-lightbox="animation: slide">

					<?php if(ICL_LANGUAGE_CODE=='es'): ?>
					<a class="uk-button uk-button-default uk-text-bold uk-width-1-1 bit-btn"  href="https://www.youtube.com/watch?v=vmxteDC4i1g" > dentro vídeo  <span class="uk-text-middle " uk-icon="icon:  play-circle; ratio:1.3;" > </span></a>
					
					<?php elseif(ICL_LANGUAGE_CODE=='ca'): ?>
						<a class="uk-button uk-button-default uk-text-bold uk-width-1-1 bit-btn " href="https://www.youtube.com/watch?v=39ch7RKUtnM"> obrir vídeo <span class="uk-text-middle " uk-icon="icon:  play-circle; ratio:1.3;" > </span></a>
					
					<?php endif; ?>

				</div>
			
			</div>
			
		</div>
	
		<div class="hero-paralax uk-position-relative " style="boder: 1px solid red; ">
			
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/banner/hero-tkc.svg" alt="" 
			uk-parallax="opacity:0.1;scale:0.5; y:300" 
			class="uk-position-top-right uk-position-absolute hero-paralax-top" 
			style="">
			
			<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/banner/hero-tkc-bottom.svg" alt="" 
			uk-parallax="scale:1; y:120;viewport:0.9;" 
			class="uk-position-bottom-right uk-position-absolute hero-paralax-bottom" 
			style="">

		</div>
	
	</div>

</div> 


<script src="https://cdn.jsdelivr.net/npm/fitty@2.2.6/dist/fitty.min.js"></script>
<script>
fitty('.sub-claim',{

	multiLine:true,
	minSize : 22,
	maxSize	:30
	
	
}) ;
</script>