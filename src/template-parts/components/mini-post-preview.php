<div class=""style="background-color:rexd;" >
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
        <h5 class="uk-margin-small-top uk-margin-remove-bottom">
            <a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a>
        </h5>
        <div class="uk-text-small">
            <?php the_excerpt(); ?>
        </div>
    </div> 