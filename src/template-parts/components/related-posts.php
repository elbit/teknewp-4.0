<?php $posts = get_field('related_posts', false, false); ?>

<?php $loop = new WP_Query(array('post_type' => 'post', 'posts_per_page' => 3, 'post__in' =>
$posts, 'post_status' => 'publish', 'orderby' => 'post__in', 'order' => 'ASC' )); ?>

<?php if($loop->have_posts()) { ?>
    <hr>
   <div class=" uk-grid-small uk-child-width-expand@s uk-grid-match uk-grid-divider" uk-grid>
    
   <?php while ($loop->have_posts()) : $loop->the_post(); ?>

    <div class=""style="background-color:rexd;" >
        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
        <h5 class="uk-margin-small-top uk-margin-remove-bottom">
             <a href="<?php the_permalink(); ?>"> <?php the_title(); ?> </a>
        </h5>
        <div class="uk-text-small">
        <?php the_excerpt(); ?>
        
        </div>
       
        
        
    </div> 
        
      <?php endwhile; ?>

    </div>
    <hr>
<?php } wp_reset_query(); ?>

