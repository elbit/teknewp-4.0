
<h3 class="" style="margin-bottom:0"><?php pll_e('Otros artículos recomendados:'); ?></h3>
<hr class="uk-margin-small-top">
<div class="uk-grid-small uk-child-width-1-3 uk-grid-atch uk-grid-divider" uk-grid>

<?php 
$posts = get_field('related_posts');
if( $posts ): ?>

<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); ?>
        
        <?php get_template_part( '/template-parts/components/mini-post-preview' ); ?>

<?php endforeach; ?>


<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>

<?php else: ?>

<?php
$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 3, 'post__not_in' => array($post->ID) ) );
if( $related ) foreach( $related as $post ) {
setup_postdata($post); ?>
 
 <?php get_template_part( '/template-parts/components/mini-post-preview' ); ?>
 
<?php }
wp_reset_postdata(); ?>

<?php endif; ?>

</div>
<hr>


