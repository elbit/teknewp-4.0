
<a class="uk-navbar-item uk-logo" href="<?php //echo esc_url( home_url() ); ?>">
	<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-teknedata.svg" alt="Logo" class="logo-img">
</a>
</div>

<ul class="uk-navbar-nav  bit-navbar-center bit-navbar-center-tkd">
	<li class=" nav-item-hide show-me-2"">
		<a class="event-tkd" href="<?php echo esc_url( home_url( '/' ) ); ?>" class="" tabindex="0">teknecultura</a>
	</li>
	<li class=" nav-item-hide show-me-3">
		<a class="event-team" href="#team" uk-scroll="offset:60;">
			
			<?php pll_e('equipo'); ?>
		</a>
	</li>
	<li class=" nav-item-hide show-me-4">
		<a class="event-clients" href="#clients" uk-scroll="offset:100;">
			
			<?php pll_e('clientes'); ?>
		</a>
	</li>
	<li class=" nav-item-hide show-me-5">
		<a class="event-casos" href="<?php echo esc_url( home_url( '/' ) ); ?><?php pll_e('bloc/estudio-caso-festival-grec-500-roi-aplicando-segmentacion-por-fidelidad-las-campanas-email-marketing/'); ?>">
			
			<?php pll_e('casos'); ?>
		</a>
	</li>
	
	<li class=" nav-item-hide show-me-6">
		<a class="event-blog" href="<?php echo esc_url( home_url( '/' ) ); ?><?php pll_e('blog/');?>" uxk-scroll="offset:60;">
			
			<?php pll_e('blog'); ?>
		</a>
	</li>