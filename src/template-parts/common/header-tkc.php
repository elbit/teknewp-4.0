	<a class="uk-navbar-item uk-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>">
		<img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo-tekne.svg" alt="Logo" class="logo-img">
	</a>
</div><!-- //end nav left -->

<ul class="uk-navbar-nav bit-navbar-center nav-item-hide-wrapper " uk-scrollspy-nav="closest: li; scroll: true; offset: 100" style="boder: 1px solid green" >
	<li  class=" nav-item-hide show-me-2">
		<a class="event-tkd" href="<?php echo esc_url( home_url( '/' ) ); ?><?php pll_e('teknedata/'); ?>" tabindex="0">
			teknedata
		</a>
	</li>
	<li  class=" nav-item-hide show-me-3">
		<a class="event-team" href="<?php echo esc_url( home_url( '/' ) ); ?>#team" uk-scroll="offset:60;" >
			
			<?php pll_e('equipo'); ?>
		</a>
	</li>
	<li  class=" nav-item-hide show-me-4">
		<a class="event-clients" href="<?php echo esc_url( home_url( '/' ) ); ?>#clients" uk-scroll="offset:100;" >
			
			<?php pll_e('clientes'); ?>
		</a>
	</li>
	<li  class=" nav-item-hide show-me-5">
		<a class="event-casos" href="<?php echo esc_url( home_url( '/' ) ); ?><?php pll_e('bloc/estudio-caso-festival-grec-500-roi-aplicando-segmentacion-por-fidelidad-las-campanas-email-marketing/'); ?>" >
			
			<?php pll_e('casos'); ?>
		</a>
	</li>
	
	<li  class=" nav-item-hide show-me-6">
		<a class="event-blog" href="<?php echo esc_url( home_url( '/' ) ); ?><?php pll_e('blog/');?>" uxk-scroll="offset:60;" >
			
			<?php pll_e('blog'); ?>
		</a>
	</li>