<?php get_header(); ?>

	<section class="uk-section uk-section-small">

			<div class="uk-container uk-container-small">

			<!-- article -->
			<article id="post-404">

				<h1><?php esc_html_e( 'Page not found', 'html5blank' ); ?></h1>
				<h2>
					<a href="<?php echo esc_url( home_url() ); ?>"><?php esc_html_e( 'Return home?', 'html5blank' ); ?></a>
				</h2>

			</article>
			<!-- /article -->

		</section>
		<!-- /section -->
	</div>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
