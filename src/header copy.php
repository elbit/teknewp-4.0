<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) { echo ' : '; } ?><?php bloginfo( 'name' ); ?></title>
		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/img/favicon/favicon.ico" rel="shortcut icon">
		<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/img/favicon/touch.png" rel="apple-touch-icon-precomposed">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'rss2_url' ); ?>" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo( 'description' ); ?>">
		
		<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,600" rel="stylesheet" async>
		
		<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.19/js/uikit.min.js" ></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.19/js/uikit-icons.min.js" async></script>
	
		
		<?php if ( is_page_template( 'template-form.php' ) ) {
		$directory =  esc_url( get_template_directory_uri() );
		
		echo '<link rel="stylesheet" type="text/css" href="' . $directory . '/js/lib/FullscreenForm/css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="' . $directory . '/js/lib/FullscreenForm/css/demo.css" />
		<link rel="stylesheet" type="text/css" href="' . $directory . '/js/lib/FullscreenForm/css/component.css" />
		<link rel="stylesheet" type="text/css" href="' . $directory . '/js/lib/FullscreenForm/css/cs-select.css" />
		<link rel="stylesheet" type="text/css" href="' . $directory . '/js/lib/FullscreenForm/css/cs-skin-boxes.css" />
		<script src="' . $directory . '/js/lib/FullscreenForm/js/modernizr.custom.js"></script>' ;}?>
		
		<?php wp_head(); ?>
		
		
		<!-- //fout fluid.js -->
		<style>.sub-claim { 
  				display: inline-block;}
  		</style>
		
		<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-36720546-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-36720546-1');
</script>

	</head>
	<body <?php body_class(); ?>>
		
		
		<header class="header uk-padding-xsmall-v bit-header" role="banner"  uk-sticky uk-parallax="background-color: white,#ECEBEB;">
			
			<nav class="uk-navbar-container bit-nav" uk-navbar uk-parallax="background-color: white,#ECEBEB;" >
				
				<div class="uk-navbar-left bit-nav-l" style="" >
					
					<?php	if ( is_page_template( 'front-page-teknedata.php' ) ) {
						get_template_part( 'template-parts/common/header-teknedata' );
						
						}
						else if ( is_front_page() ){
						
							get_template_part( 'template-parts/common/header-tkc' );
						}
						else {
						
							get_template_part( 'template-parts/common/header-pages' );
						}
						?>
						
						
						<li class="nav-item-hide show-me-7">
							<a class="event-contacto" href="<?php echo esc_url( home_url( '/' ) ); ?><?php pll_e('contacto'); ?>/">
								
								<?php pll_e('contacto'); ?>
							</a>
						</li>
						
						<li class="nav-item-hide show-me-8 ">
							<a class="event-subscribe" href="" class="uk-icon-link" uk-icon="icon: triangle-down; ratio: 0.9">
								<?php pll_e('suscríbete'); ?>
							</a>
							<div uk-drop="mode: click;  offset: -5" uk-drop="" class="uk-background-muted uk-card  uk-padding-xsmall uk-box-shadow-small">
							<?php get_template_part( 'template-parts/common/header-mailchimp-form' );?>
								
							</div>
						</li>
				
					</ul>
				
			
				<div class="bit-nav-r uk-width-expand uk-flex uk-flex-right uk-flex-middle " style="boder: 1px solid blue">
					
					<ul class="bit-switch-lang uk-text-small uk-flex uk-flex-right uk-flex-center show-me-1-lg" style="margin-bottom:0;">
					
						<?php pll_the_languages(); ?>
						
					</ul>
					
					<div class="uk-flex uk-flex-right show-me-1-lg tkdbi">
						<a class="bit-nav-r-btn uk-button uk-button-small event-tkdbi" href="https://bi.teknedata.com/login?ReturnUrl=%2F" target="_blank" rel=”noopener noreferrer”><?php pll_e('acceso clientes'); ?></a>
					</div>
				
				</div>
			
			
			</nav>

			
			<span class="rwd-wrp nav-item-hide">
			<a class="btn-menu-rwd uk-position-top-left uk-text-bold" uk-toggle="target: .nav-item-hide; cls: show-me; animation: uk-animation-fade; queued: false" style="z-index:1000;"><span class="ham"uk-icon="icon: menu; ratio: 1.5"></span><span class="bit-close" uk-icon="icon: close; ratio: 1.5" style="z-index:1000;"></span></a>
			</span>
		
		
		</header>


