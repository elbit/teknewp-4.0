<?php get_header(); ?>
<main role="main" aria-label="Content" class="">
	
	<section class="uk-section uk-section-small">
		
		<div class="uk-container uk-container-xsmall">
			
			<?php if ( have_posts() ) : while (have_posts() ) : the_post(); ?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<div class="date">
					
					<?php echo get_avatar( get_the_author_meta( 'ID' ), 50 ); ?>
					
					<time datetime="<?php the_time( 'Y-m-d' ); ?> <?php the_time( 'H:i' ); ?>">
					<?php the_date(); ?> </time>
					
					<span class="author"><?php esc_html_e( 'por', 'html5blank' ); ?> <?php the_author_posts_link(); ?></span>

				</div>
				
				<h1 class="bit-post-title uk-margin-remove-bottom uk-margin-medium-top"><?php the_title(); ?></h1>
				
				<span><?php the_tags('# '); ?></span>
				
				<?php the_content();  ?>
				
				<?php edit_post_link();  ?>
				<?php //comments_template(); ?>
			</article>

			
			
			<?php endwhile; ?>
			
			<?php else : ?>
			
			<article>
				<h1><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>
			</article>
			
			<?php endif; ?>

			<div>
				<?php get_template_part( '/template-parts/components/related-posts-2' ); ?>
			</div>
		</section>
	</div>
	
</main>
<?php get_footer(); ?>