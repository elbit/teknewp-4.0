<?php  /* Template Name: Teknedata Front Page */get_header(); ?>

<section class="uk-padding-top home-banner">
	<?php get_template_part( 'template-parts/front-page/teknedata/hero-teknedata' ); ?>
</section>

<section id="project" class="uk-section home-content" style="padding-top: 0;">
	<?php get_template_part( 'template-parts/front-page/teknedata/home-content-teknedata' ); ?>
</section>


<section id="" class="uk-section uk-section-xsmall home-switch-banner uk-background-primary-soft ">
	<?php get_template_part( 'template-parts/front-page/teknedata/home-switch-banner-teknecultura' ); ?>
</section>


<?php get_template_part( 'template-parts/front-page/shared/front-page-shared' ); ?>

<?php get_footer(); ?>