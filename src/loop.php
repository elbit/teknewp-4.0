<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	
	<article class="" id="post-<?php the_ID(); ?>" <?php post_class('uk-margin-small-top'); ?>>

		
		<?php if ( has_post_thumbnail() ) : ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
				<?php the_post_thumbnail( ); ?>
			</a>
		<?php endif; ?>
		

		
		<h2 class=" uk-margin-remove-bottom">
			<a class="uk-text-bold" href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
		</h2>
		

		
		<span class="date uk-text-small">
			<time datetime="<?php the_time( 'Y-m-d' ); ?> <?php the_time( 'H:i' ); ?>">
				<?php the_date(); ?> 
			</time>
			<?php esc_html_e( '', 'html5blank' ); ?> <?php the_author_posts_link(); ?>
		</span>
		

		<?php the_excerpt();  ?>

		<?php edit_post_link(); ?>
		<hr>
	
	</article>

	

<?php endwhile; ?>

<div class="nav-previous alignleft"><?php //previous_posts_link( 'tornar' ); ?></div>
<div class="nav-next alignright"><?php //next_posts_link( 'més entrades antigues' ); ?></div>

<?php else : ?>

	
	<article>
		<h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
	</article>
	

<?php endif; ?>
