	<section class="uk-section-small uk-background-muted">
		<footer class="uk-container uk-container-expand" role="contentinfo">
			
			<div class=" uk-text-small uk-text-center">
				&copy; <?php echo esc_html( date( 'Y' ) ); ?> Copyright <?php bloginfo( 'name' ); ?> // <a href="mailto:info@teknecultura.com "><span uk-icon="icon:mail;ratio:0.9;"></span> info@teknecultura.com </a> // <a href="tel:686690249"><span uk-icon="icon:phone;ratio:0.9;"></span> 686 690 249 </a> //

				<a href="https://twitter.com/teknecultura" target="_blank" rel=”noopener noreferrer”> <span uk-icon="icon:twitter;ratio:0.8;"></span> Twitter</a><br>
				
				<a href="<?php echo esc_url( home_url( '/' ) ); ?><?php pll_e('aviso-legal-web/'); ?>"><?php pll_e('Política de privacidad'); ?></a> // 
				<a href="<?php echo esc_url( home_url( '/' ) ); ?><?php pll_e('politica-de-cookies/'); ?>">Cookies</a> // 
				<a href="<?php echo esc_url( home_url( '/' ) ); ?><?php pll_e('politica-privacidad-las-redes-sociales/'); ?>"><?php pll_e('Política redes sociales'); ?></a>
			</div>
			
		</footer>
	</section>

	
	<?php wp_footer(); ?>



	
</body>
</html>