<!-- search -->
<form class="search " method="get" action="<?php echo esc_url( home_url() ); ?>" style="width:100%;">
	
	<div role="search" class="uk-flex uk-flex-between uk-flex-wrap" >
		
		<input class="search-input uk-input uk-width-1-1 uk-width-2-3@s " type="search" name="s" aria-label="Search site for:" placeholder="<?php esc_html_e( 'Para buscar, escribir y pulsar enter', 'html5blank' ); ?>" >
		
		<button class="search-submit uk-button uk-button-default uk-button-medium uk-padding-2  uk-width-1-3@s uk-background-muted
		uk-width-1-1
		
		"  type="submit" style="wixdth:20%"> <i uk-icon="icon:search" > </i> <?php esc_html_e( 'Search', 'html5blank' ); ?></button>
	</div>

</form>

