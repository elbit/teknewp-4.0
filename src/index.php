<?php get_header(); ?>
<main role="main" aria-label="Content ">

	<section class="uk-section uk-section-small">
		<div class="uk-container" id="infinite-container">
			
			<div id=""class="uk-child-width-1-2@m uk-grid-large uk-grid-match "uk-grid>
			
				<?php get_template_part( 'loop' ); ?>
			
			</div>
		</div>
	</section>
	
</main>


<?php get_footer(); ?>